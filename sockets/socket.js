const { io } = require('../index');
const Band = require('./models/band');
const Bands = require('./models/bands');

//Creo una nueva instancia de la clase Bands() importandola y agrego algunas bandas
const bands = new Bands();

bands.addBand(new Band('Queen'));
bands.addBand(new Band('Scorpions'));
bands.addBand(new Band('Medina Azahara'));
bands.addBand(new Band('Chingones'));

console.log(bands);

// Mensajes de Sockets
io.on('connection', (client) => {
    console.log('Cliente conectado');

    client.on('disconnect', () => {
        console.log('Cliente desconectado');
    });

    client.on('mensaje', (payload) => {
        console.log('Mensaje', payload);

        io.emit('mensaje', { admin: 'Nuevo mensaje' });
    });

    //Aqui el server escucha el mensaje emitido por flutter  con clave 'emitir-mensaje'
    //y el server luego lo remite a todos  los clientes que esten conectados, en este caso el navegador
    client.on('emitir-mensaje', (payload) => {
        console.log('Mensaje desde Flutter ', payload);
        client.broadcast.emit('nuevo-mensaje', payload);
    });

    //Nota: Cuando un cliente se conecta puedo hacerle una emision de todas
    //las bandas registradas actualmente en el server o servidor.
    //flutter estara escuchando este evento desde HomePage() para recibir la data
    client.emit('active-bands', bands.getBands());

    //Escucha  del evento emitido en el cliente flutter desde la Clase HomePage(),
    //en el Widget _bandTile()
    client.on('vote-band', (payload) => {
        console.log(payload);
        //Tratamiento para los votos de cada banda.
        bands.voteBand(payload.id);
        //Debo actualizar la lista de bandas con los valors nuevos debido a las votaciones de
        //cada banda,todos los clientes conectados estan en io.
        io.emit('active-bands', bands.getBands());
    });

    //Escuchamos el evento add-band emitido desde el HomePage()
    client.on('add-band', (payload) => {
        const newBand = new Band(payload.name);
        bands.addBand(newBand);
        io.emit('active-bands', bands.getBands());
    });


    //Escuchamos el evento generado en el Cliente flutter, que elimina una banda 
    client.on('delete-band', (payload) => {
        console.log('Banda eliminada', payload.id);
        bands.deleteBand(payload.id);
        io.emit('active-bands', bands.getBands());
    });

    // client.on('emitir-mensaje', (payload) => {
    //     io.emit('nuevo-mensaje', payload);
    // });
});