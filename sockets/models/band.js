const { v4: uuidv4 } = require('uuid');

class Band {
    constructor(name = 'no-name') {
        //Para trabajar con el id , instalamos el paquete
        //uuid con npm...npm  uuid
        this.id = uuidv4();
        this.name = name;
        this.votes = 0;
    }
}
//Para exportar la clase Band(), debo hacerlo con la siguiente linea de codigo
module.exports = Band;