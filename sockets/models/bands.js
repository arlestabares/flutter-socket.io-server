//clase encargada de manejar las bandas

const Band = require('./band');

class Bands {
    constructor() {
        this.bands = [];
    }
    addBand(band = new Band()) {
        this.bands.push(band);
    }
    getBands() {
        return this.bands;
    }
    deleteBand(id = '') {
        //Para eliminar una banda por id, filtro todas las bandas
        //y regreso todas las que no cumplan la condicion, osea el id
        this.bands = this.bands.filter(band => band.id != id);

        return this.bands;
    }

    //Metodo encargado de los votos de cada una de las bandas del arreglo.
    //se ejecutara cada vez que pinche una banda aumentando su valor en votos
    voteBand(id = '') {
        //El map en java script lo que hace es transformar la band que recibe
        this.bands = this.bands.map(band => {
            //regreso un nuevo objeto que hace parte de la banda
            if (band.id == id) {
                band.votes++;
                return band;
            } else {
                return band;
            }
        });
    }
}
//Para poder utilizar este archivo en cualquier otra clase lo exporto con el module.
module.exports = Bands;